# Two Borders

A simple theme for XFWM4 that strives to reimplemnt something similar
to the look of [2bwm](https://github.com/venam/2bwm).

It utilises your GTK colours to set boarders.
